KELOMPOK E14

- Adelya Gabriel - 190639975
- Muhammad Arsyanda Tjandra -1906500154
- Prinindya Aisha Sumari - 1906399165
- Rifanti Putri Tallisha - 1906399253
- Syah Reva Fahlevi Daulay - 1906400116

Kami kelompok E14 membuat sebuah website aplikasi dengan nama Susun Deadline. Dengan diberlakukannya PJJ di UI, maka perkuliahan diadakan secara online. Dengan perkuliahan online ini mahasiswa diberikan berbagai macam tugas dengan jumlah yang tidak sedikit. Kami memutuskan untuk membuat aplikasi ini. Kegunaan dari aplikasi ini adalah untuk membantu teman-teman mahasiswa mencatat tanggal-tanggal penting di mana adanya deadline submisi tugas mata kuliah yang ada.
Fitur yang tersedia di aplikasi ini diantaranya adalah user account, important date, dan ringkasan deadline.
wireframe dari aplikasi kami bisa dilihat di: https://wirefwire.cc/pro/pp/c260ab6c4385361
situs kami bisa dilihat di: https://susun-jadwal-tk2.herokuapp.com/