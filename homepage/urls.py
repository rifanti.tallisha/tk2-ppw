from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('quotes', views.cetakquote, name="cetakquote"),
    path('formquotes', views.quotes, name="quotes"),
    path('quotes', views.cetakquote, name="cetakquote"),
]