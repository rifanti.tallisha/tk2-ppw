from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import homepage, quotes, cetakquote

class TestQuotes(TestCase):
    
    # cek views
    def test_views(self):
        found = resolve('/quotes')
        self.assertEqual(found.func, cetakquote)

    def test_views2(self):
        found = resolve('/formquotes')
        self.assertEqual(found.func, quotes)



