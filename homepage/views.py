from django.shortcuts import render
from django.http import HttpResponseRedirect
# Create your views here.
from .forms import *
from .models import formquote

def homepage(request):
    return render(request, 'homepage.html')

def quotes(request):
    if(request.method=='POST'):
        qts = QuoteForm(request.POST or None)
        if qts.is_valid():
            qts.save()
            return HttpResponseRedirect("/quotes")
    response = {'form_quote' : QuoteForm}
    return render(request, 'FormQuotes.html', response)

def cetakquote(request):
    infoquote = formquote.objects.all()
    print(infoquote)
    response = {'quotesnya' : infoquote}
    return render(request, 'quotes.html', response)





    