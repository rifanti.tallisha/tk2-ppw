from django import forms
#import model dari models.py
from .models import formquote
#lisa:

class QuoteForm(forms.ModelForm):
    class Meta:
        model = formquote
        fields = '__all__'

        widgets = {
            'Quote' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Tuliskan Quote Motivasi'}),
            'nama' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Tuliskan Nama'}),
        }

    error_messages = { 'required' : 'Please Type' }

    input_attrs = {
        'type' : 'text',
    }



