# Create your views here.
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import FeedbackForm
from .models import formfdk
from django.http import HttpResponse, JsonResponse
import json

# Create your views here.
def feedback(request):
    if request.method=='POST':
        fdk = FeedbackForm(request.POST)
        if fdk.is_valid():
            fdk.save()
            return HttpResponseRedirect("/feedback")
    response = {'feedback_form' : FeedbackForm}
    return render(request, 'feedback.html', response)

def ViewFeedback(request):
    feedback = formfdk.objects.all()

    context = {
        'nama' : 'nama',
        'email' : 'email',
        'feedback' : 'feedback',
    }
    return render(request, "feedback.html", context)  

def getFeedback(request):
    userReq =  formfdk.objects.all().values()
    lst = list(userReq)
    #print(lst)
    return JsonResponse(lst, safe=False)


