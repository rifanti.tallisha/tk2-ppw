from django.db import models

# Create your models here.
class formfdk(models.Model):
    nama = models.CharField(max_length=60)
    email = models.EmailField()
    feedback = models.CharField(max_length=500)

    def __str__(self):
        return self.feedback
