from django import forms

#import model dari models.py
from .models import formfdk

class FeedbackForm(forms.ModelForm):
    class Meta:
        model = formfdk
        fields = [
            'nama',
            'email',
            'feedback',
        ]     

        widgets = {
            'nama' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Tuliskan Nama Anda'}),
            'email' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Tuliskan Email Anda'}),
            'feedback' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Tuliskan Feedback Anda'}),
        }
