from django.urls import path
from . import views

app_name = 'feedback'

urlpatterns = [
    path('', views.feedback, name='feedback'),
    path('feedback', views.feedback, name='feedback'),
    path('viewfeedback', views.ViewFeedback, name='viewfeedback'),
]
