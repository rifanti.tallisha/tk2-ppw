from django.test import TestCase, tag, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from selenium import webdriver
from .views import feedback, getFeedback, ViewFeedback
from .models import formfdk
from .forms import FeedbackForm

# Create your tests here.

class FeedbackTest(TestCase):
    def test_feedback_is_exist(self):
        response = Client().get('/feedback/')
        self.assertEqual(response.status_code,200)


