from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('login/', views.masuk, name = 'login'),
    path('daftar/', views.baru, name='daftar'),
    path('logout/', views.keluar, name='logout'),
]