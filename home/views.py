from django.shortcuts import render, redirect
from .forms import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect

# Create your views here.
def masuk(request):
    if request.method == "POST":
        form = buatlogin(request.POST)
        if form.is_valid():
            usrnm = form.cleaned_data["username"]
            psswrd = form.cleaned_data["password"]
            check = authenticate(request, username=usrnm, password=psswrd)
            if check is not None:
                login(request, check)
                return redirect('/')
            else:
                return redirect('/home/daftar/')
    else:
        form = buatlogin()
        response = {"form" : form}
        return render(request, "login.html", response)

def baru(request):
    if request.method == "POST":
        form = buatbaru(request.POST)
        if form.is_valid():
            usrnm = form.cleaned_data["username"]
            emailnya = form.cleaned_data["email"]
            psswrd = form.cleaned_data["password"]
            try:
                user = User.objects.get(username = usrnm)
                return redirect("/home/daftar/")
            except User.DoesNotExist:
                user = User.objects.create_user(usrnm, emailnya, psswrd)
                return redirect("/home/login")
    else:
        form = buatbaru()
        response = {"form":form}
        return render(request, "signup.html", response)

def keluar(request):
    logout(request)
    return redirect('/')
