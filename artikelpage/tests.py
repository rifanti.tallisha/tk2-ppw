from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from artikelpage.models import *
from artikelpage.forms import *
from artikelpage.views import *

# Create your tests here.
class ArtikelPageTest(TestCase):
    
    def test_artikelpageapp_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)