var likehtml;
var response;
var isSearched = false;
var search;

$(document).ready(function () {
    $("#searchForm").submit(function () {
        search = $("#searchInput").val();
        if (search == "") {
            alert("Please enter something in the field first");
        } else {
            $("#result").text("");
            isSearched = true;
            var img = "";
            var name = "";
            var info = "";
            var url = "";
            var starttable =
                '<table class="table table-borderless table-dark" style="border-radius: 15px;">' +
                "<thead>" +
                "<tr>" +
                '<th scope="col">#</th>' +
                '<th scope="col">Image</th>' +
                '<th scope="col">Artist</th>' +
                '<th scope="col">Info</th>'+
                '<th scope="col">URL</th>' +
                
                "</tr>" +
                "</thead>" +
                "<tbody>";
            var endtable = "</tbody> </table>";
            var html = "";

            $.get(
                "https://ws.audioscrobbler.com/2.0/?method=artist.search&artist="+search+"&api_key=6cb4899a2a295ec27de692cd4a721b0b&format=json",
                function (response) {
                    console.log(response);
                    if (!response.results.artistmatches.artist.length) {
                        $("#result").html(
                            "<h2>'" + search + "' not found.</h2>"
                        );
                    }
                    html += starttable;
                    for (
                        i = 0;
                        i < response.results.artistmatches.artist.length;
                        i++ //items is name of array
                    ) {
                        name = response.results.artistmatches.artist[i].name;
                        img = "<img src='"+response.results.artistmatches.artist[i].image[0]["#text"]+"'>";
                        url = "<a href = '"+response.results.artistmatches.artist[i].url+"'>Link</a>";
                        info = '<button ' + 
                        "onclick='modal(\""+name+"\")' " + 
                        'id="modalbutton" '+
                        'type="button" ' +
                        'class="btn btn-success btn-sm" '+ 
                        'name="'+name+'" '+
                        'data-toggle="modal" '+
                        'data-target="#exampleModal" '+
                        '>Info</button>';

                        console.log(name);
                        console.log(img);
                        
                        row =
                            '<tr id="row' +
                            i +
                            '"> <th scope="row">' +
                            (i + 1) +
                            "</th> " +
                            '<td id="img' +
                            i +
                            '">' +
                            img +
                            "</td> " +
                            '<td id="title' +
                            i +
                            '">' +
                            name +
                            "</td> " +
                            "<td>" +
                            info +
                            "</td> " +
                            '<td id="url' +
                            i +
                            '">' +
                            url +
                            "</td> " +
                            "</tr>";
                        html += row;
                    }
                    html += endtable;
                    $("#result").append(html);
                }
            );
        }
        return false;
    });
});
function modal(name) {
    $("#exampleModalLabel").text(name);
    $("#albums").text("");
    $("#songs").text("");
    var htmlalbum = "";
    var albumstart =
        '<table class="table table-borderless table-dark" style="border-radius: 15px;">' +
        "<thead>" +
        "<tr>" +
        '<th scope="col">#</th>' +
        '<th scope="col">Image</th>' +
        '<th scope="col">Album</th>' +
        '<th scope="col">URL</th>' +
        "</tr>" +
        "</thead>" +
        "<tbody>";
    htmlalbum += albumstart;
    $.get(
        "https://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=" +
            name +
            "&api_key=6cb4899a2a295ec27de692cd4a721b0b&format=json",
        function (response) {
            for (i = 0; i < response.topalbums.album.length; i++) {
                var img =
                    "<img src='" +
                    response.topalbums.album[i].image[0]["#text"] +
                    "'>";
                var url =
                    "<a href = '" +
                    response.topalbums.album[i].url +
                    "'>Link</a>";
                row =
                    '<tr id="row' +
                    i +
                    '"> <th scope="row">' +
                    (i + 1) +
                    "</th> " +
                    '<td id="img' +
                    i +
                    '">' +
                    img +
                    "</td> " +
                    '<td id="title' +
                    i +
                    '">' +
                    response.topalbums.album[i].name +
                    "</td> " +
                    '<td id="url' +
                    i +
                    '">' +
                    url +
                    "</td> " +
                    "</tr>";
                htmlalbum += row;
            }
            htmlalbum += "</tbody> </table>";
            $("#albums").append(htmlalbum);
        }
    );
    var htmlsongs = "";
    var songsstart =
        '<table class="table table-borderless table-dark" style="border-radius: 15px;">' +
        "<thead>" +
        "<tr>" +
        '<th scope="col">#</th>' +
        '<th scope="col">Image</th>' +
        '<th scope="col">Song</th>' +
        '<th scope="col">URL</th>' +
        "</tr>" +
        "</thead>" +
        "<tbody>";
    htmlsongs += songsstart;
    $.get(
        "https://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&artist=" +
            name +
            "&api_key=6cb4899a2a295ec27de692cd4a721b0b&format=json",
        function (response) {
            for (i = 0; i < response.toptracks.track.length; i++) {
                var img =
                    "<img src='" +
                    response.toptracks.track[i].image[0]["#text"] +
                    "'>";
                var url =
                    "<a href = '" +
                    response.toptracks.track[i].url +
                    "'>Link</a>";
                row =
                    '<tr id="row' +
                    i +
                    '"> <th scope="row">' +
                    (i + 1) +
                    "</th> " +
                    '<td id="img' +
                    i +
                    '">' +
                    img +
                    "</td> " +
                    '<td id="title' +
                    i +
                    '">' +
                    response.toptracks.track[i].name +
                    "</td> " +
                    '<td id="url' +
                    i +
                    '">' +
                    url +
                    "</td> " +
                    "</tr>";
                htmlsongs += row;
            }
            htmlsongs += "</tbody> </table>";
            $("#songs").append(htmlsongs);
        }
    );
}
