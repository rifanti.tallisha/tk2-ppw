from django.apps import AppConfig


class ArtikelpageConfig(AppConfig):
    name = 'artikelpage'
