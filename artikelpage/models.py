from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.shortcuts import redirect
from datetime import datetime,date
from ckeditor.fields import RichTextField

# Create your models here.

# class Author(models.Model):
#     name = models.CharField(max_length=255)
#     email = models.CharField(max_length=255)

#     def __str__(self):
#         return self.name

class Post(models.Model):
    title = models.CharField(max_length=255)
    author = models.ForeignKey(User,on_delete=models.CASCADE)
    body = RichTextField(blank=True,null=True)
    article_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.title + '  |  ' +str(self.author)

    def get_absolute_url(self):
        # return reverse('artikelpage:article-detail', args=(str(self.id)))
        return reverse('artikelpage:artikel')