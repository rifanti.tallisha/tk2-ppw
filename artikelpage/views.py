from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Post
from .forms import PostForm, EditForm
from django.urls import reverse_lazy

# Create your views here.
# def artikel(request):
#     return render(request,'articlepage.html')

class HomeView(ListView):
    model = Post
    template_name = 'articlepage.html'
    ordering = ['article_date']

class ArticleDetailView(DetailView):
    model = Post
    template_name = 'articledetail.html'

class AddPostView(CreateView):
    model = Post
    template_name = 'addarticle.html'
    form_class = PostForm
    # fields = '__all__'

class UpdatePostView(UpdateView):
    model = Post
    template_name = 'updatearticle.html'
    form_class = EditForm
    # fields = ['title', 'body']

class DeletePost(DeleteView):
    model = Post
    template_name = 'deletearticle.html'
    success_url = reverse_lazy('artikelpage:artikel')

def music(request):
    return render(request,'music.html',{})

