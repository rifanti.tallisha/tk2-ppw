from django.urls import path
from .views import HomeView, ArticleDetailView, AddPostView, UpdatePostView, DeletePost
from . import views

app_name = 'artikelpage'

urlpatterns = [
    path('', HomeView.as_view(), name='artikel'),
    path('artdetail/<int:pk>', ArticleDetailView.as_view(), name='article-detail'),
    path('addarticle/', AddPostView.as_view(), name='add_article'),
    path('artdetail/edit/<int:pk>', UpdatePostView.as_view(), name='update'),
    path('artdetail/delete/<int:pk>', DeletePost.as_view(), name='delete'),
    path('searchMusic/', views.music, name='music'),
]