from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.db.models import Q
# Create your views here.
from .forms import *
from .models import PostModel

def index(request):
    posts = PostModel.objects.all()
    context = {
        'heading' : 'Post',
        'contents': 'List Post',
        'posts' : posts,
    }
    return render(request, 'daftarjadwal.html', context)

def create(request):
    post_form = PostForm(request.POST or None)
    if request.method == 'POST':
        #cara ambil data dari input user ke database dg validasi
        if post_form.is_valid():
            post_form.save()
        return HttpResponseRedirect("daftarjadwal")

    
    context = {
        'pageTitle' : 'Create Form',
        'post_form' : post_form,
    }
    return render(request, 'susunjadwal.html', context)

def update(request, update_id):
    akun_update = PostModel.objects.get(id=update_id)
    data = {
        'namaMatkul' : akun_update.namaMatkul,
        'namaTugas' : akun_update.namaTugas,
        'semester' : akun_update.semester,
        'Deadline' : akun_update.Deadline,
    }
    post_form = PostForm(request.POST or None, initial=data, instance=akun_update)

    if request.method == 'POST':
        #cara ambil data dari input user ke database dg validasi
        if post_form.is_valid():
            post_form.save()
        return HttpResponseRedirect("/daftarjadwal/daftarjadwal")

    
    context = {
        'pageTitle' : 'Update Form',
        'post_form' : post_form,
    }
    return render(request, '/susunjadwal.html', context)

def delete(request, delete_id):
    PostModel.objects.filter(id=delete_id).delete()
    return HttpResponseRedirect("/daftarjadwal/daftarjadwal")


def cariJadwal(request):
    context = {}
    query = request.GET.get('search', False)
    context['query'] = query
    if query:
        cari = PostModel.objects.filter(Q(namaMatkul__icontains=query) |
                                       Q(namaTugas__icontains=query) )
        if cari:
            context['hasil'] = cari
        else:
            context['hasil'] = 'Tidak ditemukan'
        
    return render(request, 'cariJadwal.html', context)
