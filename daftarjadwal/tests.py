from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from daftarjadwal.models import *
from daftarjadwal.forms import *
from daftarjadwal.views import *
from datetime import date

# Create your tests here.

class SusunDeadlineTest(TestCase):

    def test_susunjadwal_is_exist(self):
        response = Client().get('/daftarjadwal/susunjadwal')
        self.assertEqual(response.status_code,200)

    def test_daftarjadwal_is_exist(self):
        response = Client().get('/daftarjadwal/daftarjadwal')
        self.assertEqual(response.status_code,200)

    def test_models_create(self):
        p1 = PostModel.objects.create(namaMatkul="PPW",namaTugas="TK1",semester="Gasal 2020/2021",Deadline= date(2021, 1, 5))
        hitungJumlah = PostModel.objects.all().count()
        self.assertEqual(hitungJumlah,1)

    def test_validate_model(self):
        test = PostModel.objects.create(namaMatkul="PPW",namaTugas="TK1",semester="Gasal 2020/2021",Deadline= date(2021, 1, 5))
        with self.assertRaises(ValidationError, msg='maafEinsteintidak bisa diposting'):
            validate_author('Einstein')

    def test_model_name(self):
        PostModel.objects.create(namaMatkul="PPW",namaTugas="TK1",semester="Gasal 2020/2021",Deadline= date(2021, 1, 5))
        postModel = PostModel.objects.get(namaMatkul="PPW")
        self.assertEqual(str(postModel),'PostModel object (1)')