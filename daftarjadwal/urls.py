from django.urls import path
from . import views

app_name = 'daftarjadwal'

urlpatterns = [
    path('susunjadwal', views.create, name='create'),
    path('daftarjadwal', views.index),
    path('cariJadwal', views.cariJadwal),
    path('update/<int:update_id>', views.update, name='update'),
    path('delete/<int:delete_id>', views.delete, name='delete'),
]