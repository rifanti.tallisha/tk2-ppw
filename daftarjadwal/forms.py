from django import forms

#import model dari models.py
from .models import PostModel

class PostForm(forms.ModelForm):
    class Meta:
        model = PostModel
        fields = [
            'namaMatkul',
            'namaTugas',
            'Deadline',
            'semester',           
        ]
        widgets = {
            'namaMatkul': forms.TextInput(  
                attrs={
                    'class' : 'form-control', 
                    'placeholder' : 'Tuliskan nama Mata kuliah',
                }
            ),
            'namaTugas': forms.TextInput(  
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Tuliskan nama tugas anda',
                }
            ),
            'semester': forms.Select(  
                attrs={
                    'class' : 'form-control', 
                }
            ),        
            'Deadline': forms.DateInput(attrs={'type':'date'}),
        }
# pada bagian ini form susun deadline memiliki tiga data yang harus disubmit oleh pengguna