from django.urls import path
from . import views

app_name = 'faq'

urlpatterns = [
    path('', views.index, name='faq'),
    path('list-faq/', views.ViewQuestion, name='list_faq'),
    path('list/', views.getQuestions, name='get_faq'),
]
