from django import forms

class QuestionBox(forms.Form):
    question = forms.CharField(max_length=1200, widget=forms.TextInput(attrs={
            'class':'form-control input-form my-2', 
            'placeholder' : 'Tanyakan disini!', 
            'style': 'height:8vh', 
            'required': 'True', 
            }))