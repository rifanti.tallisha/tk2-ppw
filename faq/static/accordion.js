$( function(){
    $('#accordion').sortable()
})

$('.button-up').click(function(){
    var thisAccordion = $(this).parent().parent().parent().parent();
    thisAccordion.insertBefore(thisAccordion.prev());
})
  
$('.button-down').click(function(){
    var thisAccordion = $(this).parent().parent().parent().parent();
    thisAccordion.insertAfter(thisAccordion.next());
})
