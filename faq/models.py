from django.db import models

# Create your models here.
class Question(models.Model):
    question = models.CharField(max_length=1200, blank=False)

    def __str__(self):
        return self.question

