from django.shortcuts import render, redirect
from .models import Question
from .forms import QuestionBox
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.http import HttpResponse, JsonResponse
import json

# @login_required(login_url=reverse_lazy('home:login'))

def index(request):
    my_form = QuestionBox()
    # questions = Question.objects.all()

    if request.method == "POST":
        my_form = QuestionBox(request.POST)
      
        if my_form.is_valid():
            Question.objects.create(**my_form.cleaned_data)
            my_form = QuestionBox()

            context = {
                'form' : my_form,
                'boolean' : True
            }
        return render(request, 'list_faq.html', context)
    else:
        my_form = QuestionBox()

        context = {
            'form' : my_form,
            'boolean' : False
        }
        return render(request, 'faq.html', context)

def ViewQuestion(request):
    questions = Question.objects.all()

    context = {
        'questions' : questions
    }
    return render(request, "list_faq.html", context)  

def getQuestions(request):
    userReq = Question.objects.all().values()
    lst = list(userReq)
    #print(lst)
    return JsonResponse(lst, safe=False)


