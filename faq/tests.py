from django.test import TestCase
from .models import Question
from django.test import Client
from django.urls import resolve
from .views import index, getQuestions, ViewQuestion

class UnitTest_FAQ(TestCase):
    def test_faq_url_exist(self):
            self.client.login(username='perry', password='iniperry')
            response = self.client.get("/faq/")
            self.assertEqual(response.status_code, 200)

    def test_check_amount_question_database(self):
        self.assertEqual(len(Question.objects.all()), 0)

    def test_models_create(self):
            p1 = Question.objects.create(question="is this a question?")
            hitungJumlah = Question.objects.all().count()
            self.assertEqual(hitungJumlah,1)

    def test_faq_page_use_right_function(self):
        function = resolve('/faq/')
        self.assertEqual(function.func, index)
        
    def test_data_json_use_right_function(self):
        found = resolve("/faq/list/")
        self.assertEqual(found.func, getQuestions)

    def test_data_json_use_right_function2(self):
        found = resolve("/faq/list-faq/")
        self.assertEqual(found.func, ViewQuestion)



